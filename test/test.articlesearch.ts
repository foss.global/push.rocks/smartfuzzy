import { expect, tap } from '@pushrocks/tapbundle';
import * as tsclass from '@tsclass/tsclass';
import * as smartfuzzy from '../ts/index';

tap.test('should sort objects', async () => {
  const articleArray: tsclass.content.IArticle[] = [
    {
      title: 'Berlin has a ambivalent history',
      content: 'it is known that Berlin has an interesting history',
      author: null,
      tags: ['city', 'Europe', 'hello'],
      timestamp: Date.now(),
      featuredImageUrl: null,
      url: null,
    },
    {
      title: 'Washington is a great city',
      content: 'it is known that Washington is one of the greatest cities in the world',
      author: null,
      tags: ['city', 'USA', 'hello'],
      timestamp: Date.now(),
      featuredImageUrl: null,
      url: null,
    },
  ];

  const testArticleSearch = new smartfuzzy.ArticleSearch(articleArray);

  const result = await testArticleSearch.search('USA');
  console.log(result);
  console.log(result[0].matches);
});

tap.start();
